/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Equipo9_Graficacion;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 *
 * @author scomp
 */
class CanvasGDX implements ApplicationListener{
    
    SpriteBatch batch;
    BitmapFont font;
    ShapeRenderer shapeRenderer;
    
    
    @Override
        public void create() {
           System.out.println("metodo crear");
           batch=new SpriteBatch();
           font=new BitmapFont();
           shapeRenderer= new ShapeRenderer();
           
        }

        @Override
        public void resize(int i, int i1) {
           System.out.println("metodo resize");
        }

        @Override
        public void render() {
        System.out.println("metodo render");
        //Limpiar color de fondo
        Gdx.gl.glClearColor(.25f, .25f, .25f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        //Dibujar
        batch.begin();
        font.draw(batch, "verano de graficacion", Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
        batch.end();
        }

        @Override
        public void pause() {
        System.out.println("metodo pause");
        }

        @Override
        public void resume() {
        System.out.println("metodo resume");
        }

        @Override
        public void dispose() {
       System.out.println("metodo dispose");
       batch.dispose();
       font.dispose();
       shapeRenderer.dispose();
        }
    
}
